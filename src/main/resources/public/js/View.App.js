define([
    'View.Router',
], function (Router) {
    return Backbone.View.extend({
        el: 'body',

        events: {
            'click a:not([data-bypass])': 'noLinkRefreshPage'
        },

        startUp: function () {
            this.router = new Router();
            this.views = {};

            Backbone.history.start({
                pushState: true
            });
        },

        noLinkRefreshPage: function (evt) {
            var link = evt.currentTarget;

            // Get the anchor href and protcol
            var href = $(link).attr("href");
            var protocol = link.protocol + "//";

            // Ensure the protocol is not part of URL, meaning its relative.
            if (href && href.slice(0, protocol.length) !== protocol &&
                href.indexOf("javascript:") !== 0) {
                // Stop the default event to ensure the link will not cause a page refresh.
                evt.preventDefault();

                // Router will take care the rest part of navigation
                Backbone.history.navigate(href, true);
            }
        },

        changeView: function (view) {
            var that = this;
            var viewName = _.str.capitalize(view.toLowerCase());

             // If view has not been loaded before, load it by requirejs
            // test
            if (!that.views[view]) {
                // Load view on demand
                require(['views/Craft.View.' + viewName], function (View) {
                    if (that.currentView) {
                        that.currentView.active = false;
                    }

                    that.currentView = new View({
                        router: that.router,
                        page: view
                    });

                    // Put in view list of app
                    if (!that.views[view]) {
                        that.views[view] = that.currentView;
                    }

                    that.currentView.active = true;
                });
            } else {
                if (that.currentView) {
                    that.currentView.active = false;
                }

                // Get view from view list
                that.currentView = that.views[view];
                that.currentView.active = true;
                that.currentView.reload();
            }
        }
    });
});