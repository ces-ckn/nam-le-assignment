var CRAFT_BEER = {
    context: window.location.pathname.substring(0, window.location.pathname.indexOf('/', 1)),
};

require([
    'View.App'
], function (App) {
    Window.app = new App();
    Window.app.startUp();
});