define(['views/Craft.View.Base'], function (BaseView) {
    return BaseView.extend({
        events: {
            'change #cbeer_name': 'handleSelectBeer',
            'click #cancel-button': 'handleCancel'
        },

        handleSelectBeer: function (evt) {
            var that = this;
            var currentId = $('#cbeer_name').val();
            this.$el.load(this.getReloadUrl('', currentId));
        },

        //Override
        render: function () {
            BaseView.prototype.render.apply(this, arguments);
        },

        //Override
        getType: function () {
            return 'beers';
        },

        // override
        onAfterSerialize: function (data) {
            // tobe overrided
            data.archive = $('#cbeer_archive').prop('checked');
        },

        handleCancel: function () {
            this.reload();
        }
    });
});