define([], function () {

    return Backbone.View.extend({
        el: '#content',
        FRAGMENT_CONTEXT: CRAFT_BEER.context + '/fragment/',
        DEFAULT_FORM_ID: '#form-rest',
        API_CONTEXT: '/api/',
        ACTION: {
            new: 'new',
            edit: 'edit'
        },

        render: function () {
            this.menu();
        },

        menu: function () {
            var that = this;

            // Activate menu right, then delegate event to handler
            $('.cbeer-menu-item-right').off('click').on('click', function (evt) {
                var $this = $(this);
                var menuId = $this.attr('id');

                if (!$this.hasClass('selected')) {
                    that.setMenu(menuId);
                    that.handleMenuEvent(evt, menuId);
                }
            });

            // Activate menu left
            $('.cbeer-menu-item').off('click').on('click', function () {
                var $this = $(this);
                $('.cbeer-menu-item').removeClass("selected");
                $this.addClass("selected");
            });
        },

        setMenu: function (menuId) {
            // Disable the selected and related menu items, active the other one
            var $menuItem = $('.cbeer-menu-item-right');
            $menuItem.removeClass('selected');
            $('#' + menuId).addClass('selected');

            // If adding, not able to delete or edit
            if (menuId == 'menu-add') {
                $('#menu-delete, #menu-edit').addClass('selected');
            }
        },

        getReloadUrl: function (action, currentId) {
            var fragmentUrl = this.FRAGMENT_CONTEXT + this.page;
            if (action || currentId) {
                fragmentUrl += '?action=' + action + '&currentId=' + currentId;
            }
            return fragmentUrl;
        },

        reload: function (action, currentId) {
            if (this.active) {
                var that = this;
                this.$el.empty();
                this.$el.load(this.getReloadUrl(action, currentId), function () {
                    if (action !== that.ACTION.new && action !== that.ACTION.edit) {
                        that.render();
                        that.setMenu('menu-save');
                    }
                });
            }
        },

        handleMenuEvent: function (evt, menuId) {
            switch (menuId) {
                case 'menu-add':
                    this.onAdd();
                    break;
                case 'menu-delete':
                    this.onDelete(menuId);
                    break;
                case 'menu-save':
                    this.onSave();
                    break;
                case 'menu-edit':
                    this.onEdit();
                    break;
                default :
            }
        },

        onAdd: function () {
            this.reload('new', '');
        },

        onEdit: function () {
            var data = this.serialize();
            this.reload('edit', data.id);
        },

        onDelete: function (menuId) {
            var that = this;
            var data = this.serialize();
            var url = this.getDeleteUrl(data.id);

            $.ajax({
                url: url,
                type: 'DELETE',
                success: function () {
                    that.reload();
                    // Menu delete backs to normal to allow the next delete
                    $('#' + menuId).removeClass('selected');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //"Failed Dependency"
                    that.handleError(errorThrown);
                }
            });
        },

        handleError: function (code) {
            var that = this;

            if (this.active) {
                var errorFragmentUrl = this.FRAGMENT_CONTEXT + "error?code=" + code;
                this.$el.empty();
                this.$el.load(encodeURI(errorFragmentUrl), function () {
                    that.render();
                    that.setMenu('menu-save');
                });
            }
        },

        getDeleteUrl: function (dataId) {
            return CRAFT_BEER.context + this.API_CONTEXT + this.getType() + '/' + dataId;
        },

        onSave: function (callback) {
            var that = this;
            var data = this.serialize();
            var url = this.getSaveUrl();
            var method;

            if (!data.id) {
                delete data.id;
                method = 'POST';
            } else {
                url += '/' + data.id;
                method = 'PATCH';
            }

            $.ajax({
                url: url,
                type: method,
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'text',
                success: function (returnData) {
                    returnData = JSON.parse(returnData);
                    that.afterSave(returnData, returnData.id);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    that.handleError(errorThrown);
                }
            });
        },

        afterSave: function (returnData, id) {
            this.reload('save', id ? id : '');
        },

        getSaveUrl: function () {
            return CRAFT_BEER.context + this.API_CONTEXT + this.getType();
        },

        serialize: function () {
            this.formId = !this.formId ? this.DEFAULT_FORM_ID : this.formId;
            var $form = $(this.formId);
            var data = {};

            // Form fields will be collected as json object
            _.each($form.serializeArray(), function (entry) {
                data[entry.name] = entry.value;
            });

            this.onAfterSerialize(data);
            return data;
        },

        onAfterSerialize: function (data) {
            // tobe overrided
        },

        getType: function () {
            // to be overrided
        },

        initialize: function (options) {
            var that = this;
            this.router = options.router;
            this.page = options.page;
            this.$el.empty();

            // Ajax template file then fill into content
            this.$el.load(this.FRAGMENT_CONTEXT + options.page, function (a, b, c) {
                that.render();
            });
        }
    });
});