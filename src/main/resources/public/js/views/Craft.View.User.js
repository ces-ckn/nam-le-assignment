define(['views/Craft.View.Base'], function (BaseView) {
    return BaseView.extend({
        events: {
            'change #cbeer_user': 'handleSelectUser',
            'click #cancel-button': 'handleCancel'
        },

        handleSelectUser: function () {
            var currentId = $('#cbeer_user').val();
            this.$el.load(this.getReloadUrl('', currentId));
        },

        //Override
        render: function () {
            BaseView.prototype.render.apply(this, arguments);
        },

        //Override
        getType: function () {
            return 'users';
        },

        afterSave: function (returnData, id) {
            var that = this;
            var userRoles = this.getUserRoles(id);
            if (userRoles && userRoles.length) {
                $.ajax({
                    url: CRAFT_BEER.context + this.API_CONTEXT + 'userRoles',
                    type: 'POST',
                    data: JSON.stringify(userRoles),
                    contentType: 'application/json',
                    success: function () {
                        that.reload('save', id ? id : '')
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        that.handleError(errorThrown);
                    }
                });
            } else {
                this.reload('save', id ? id : '');
            }
        },

        //Override
        onAfterSerialize: function (data) {
            delete data.roleId;
        },

        getUserRoles: function (userId) {
            var userRoles = [];
            var roles = $('#cbeer-user-role').val();
            if (roles.length) {
                _.each(roles, function (roleId) {
                    userRoles.push({
                        userId: userId,
                        roleId: roleId
                    });
                });
            }

            return userRoles;
        },
        handleCancel: function () {
            this.reload();
        }
    });
});