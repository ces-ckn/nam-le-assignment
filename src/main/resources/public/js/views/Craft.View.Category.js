define(['views/Craft.View.Base'], function (BaseView) {
    return BaseView.extend({
        events: {
            'change #cbeer_category': 'handleSelectCategory',
            'click #cancel-button': 'handleCancel'
        },

        handleSelectCategory: function (evt) {
            var currentId = $('#cbeer_category').val();
            this.reload('', currentId);
        },

        //Override
        render: function () {
            BaseView.prototype.render.apply(this, arguments);
        },

        //Override
        getType: function () {
            return 'categories';
        },

        //Override
        getDeleteUrl: function (dataId) {
            return CRAFT_BEER.context + this.API_CONTEXT + this.getType() + '/safe/' + dataId;
        },

        handleCancel: function () {
            this.reload();
        }
    });
});