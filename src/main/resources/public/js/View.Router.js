define([], function () {
    return Backbone.Router.extend({
        routes: {
            '': 'home',
            ':page': 'userAdministration',
        },

        home: function () { // to rename
           this.navigate('/' + LANGDING_PAGE, {replace: true, trigger: true});
        },

        userAdministration: function (page) {
            Window.app.changeView(page);
        }
    });
});