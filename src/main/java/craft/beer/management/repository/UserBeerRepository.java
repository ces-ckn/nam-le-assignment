package craft.beer.management.repository;

import craft.beer.management.entity.data.UserBeer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UserBeerRepository extends CrudRepository<UserBeer, Long> {
}
