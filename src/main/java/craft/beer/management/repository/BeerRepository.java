package craft.beer.management.repository;

import craft.beer.management.entity.data.Beer;
import craft.beer.management.entity.data.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface BeerRepository extends CrudRepository<Beer, Long> {
    @Query(value = Beer.ANYONE_QUERY, nativeQuery = true)
    List<Beer> findNoArchiveBeers();

    @Query(value = Category.CATEGORY_IN_USE_QUERY, nativeQuery = true)
    Integer countBeerInCategory(@Param("catId") Long catId);
}
