package craft.beer.management.repository;

import craft.beer.management.entity.data.UserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
    @Query(value = UserRole.DELETE_ROLES_BY_USER, nativeQuery = true)
    List<UserRole> findRolesByUserId(@Param("userId") Long userId);
}
