package craft.beer.management.configuration;

import craft.beer.management.entity.data.Beer;
import craft.beer.management.entity.data.Category;
import craft.beer.management.entity.data.Role;
import craft.beer.management.entity.data.User;
import craft.beer.management.entity.data.UserBeer;
import craft.beer.management.entity.data.UserRole;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

/**
 * Spring Data Rest is automatically configured by importing maven dependency
 *
 *         <dependency>
 *             <groupId>org.springframework.boot</groupId>
 *             <artifactId>spring-boot-starter-data-rest</artifactId>
 *         </dependency>
 *
 * This configuration is to override the available one on some settings.
 *
 * Along with Spring JPA, we can GET/POST/PUT/PATCH entities from data base by rest uri.
 * The returning entity is of JSON type and follows the HATEOAS format.
 * For example: with User entity and UserRepository declared, use this rest to access mapping entity User
 *         http://hostname:post/users
 *
 * Refer to http://projects.spring.io/spring-data-rest/ for more information
 */
@Configuration
public class RestConfiguration extends RepositoryRestMvcConfiguration {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration repositoryRestConfiguration) {
        repositoryRestConfiguration.setBaseUri("api");
        // Expose the ID property on returning json
        repositoryRestConfiguration.exposeIdsFor(User.class, Role.class, UserRole.class, Beer.class, Category.class, UserBeer.class);
        repositoryRestConfiguration.setReturnBodyOnCreate(true);
        repositoryRestConfiguration.setReturnBodyOnUpdate(true);
    }
}
