package craft.beer.management.configuration;

import craft.beer.management.security.role.CraftRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    AuthenticationProvider authenticationProvider;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    LogoutSuccessHandler logoutSuccessHandler;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/webjars/**",
                "/images/**",
                "/css/**",
                "/font/**",
                "/js/**",
                "/favicon.ico");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/api/anyone/**").permitAll()
                .antMatchers("/api/customers*").hasAnyAuthority(CraftRoles.ROLE_ADMIN.name(), CraftRoles.ROLE_ADMIN.name())
                .antMatchers("/api/users*").hasAuthority(CraftRoles.ROLE_ADMIN.name())
                .antMatchers("/api/roles*").hasAuthority(CraftRoles.ROLE_ADMIN.name())
                .antMatchers("/api/userRoles*").hasAuthority(CraftRoles.ROLE_ADMIN.name())
                .antMatchers("/api/beers*").hasAuthority(CraftRoles.ROLE_ADMIN.name())
                .antMatchers("/api/categories*").hasAuthority(CraftRoles.ROLE_ADMIN.name())
                .antMatchers("/api/beerCategories*").hasAuthority(CraftRoles.ROLE_ADMIN.name())
            .anyRequest().authenticated()
                .and()
                .csrf().disable()
                .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .defaultSuccessUrl("/home")
                .and()
                .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessHandler(logoutSuccessHandler)
                .and()
                .rememberMe()
                .userDetailsService(userDetailsService)
         ;
    }

    @Autowired
    protected void provideAuthenticationProvide(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider);
    }
}