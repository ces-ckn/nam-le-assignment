package craft.beer.management.configuration;

import craft.beer.management.security.role.CraftRoles;
import craft.beer.management.security.authentication.CraftLogoutHandler;
import craft.beer.management.security.authentication.CraftAuthenticationProvider;
import craft.beer.management.security.user.impl.CraftUserDetailsRepositoryImpl;
import craft.beer.management.security.user.UserDetailsRepository;
import craft.beer.management.service.BeerService;
import craft.beer.management.service.CategoryService;
import craft.beer.management.service.ThymeleafAbstractService;
import craft.beer.management.service.impl.BeerServiceImpl;
import craft.beer.management.service.impl.BeerThymeleafServiceImpl;
import craft.beer.management.service.impl.CategoryServiceImpl;
import craft.beer.management.service.impl.CategoryThymeleafServiceImpl;
import craft.beer.management.security.user.impl.CraftUserDetailsServiceImpl;
import craft.beer.management.service.impl.UserThymeleafServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@Configuration
public class ApplicationConfiguration {
    @Bean
    UserDetailsService craftUserDetailsService() {
        CraftUserDetailsServiceImpl userDetailsService = new CraftUserDetailsServiceImpl();
        userDetailsService.setRolesById(CraftRoles.byId());
        return userDetailsService;
    }

    @Bean
    ThymeleafAbstractService beerThymeleafService() {
        return new BeerThymeleafServiceImpl();
    }

    @Bean
    ThymeleafAbstractService categoryThymeleafService() {
        return new CategoryThymeleafServiceImpl();
    }

    @Bean
    ThymeleafAbstractService userThymeleafService() {
        return new UserThymeleafServiceImpl();
    }

    @Bean
    BeerService beerService() {
        return new BeerServiceImpl();
    }

    @Bean
    CategoryService categoryService() {
        return new CategoryServiceImpl();
    }

    @Bean
    AuthenticationProvider authenticationProvider() {
        return new CraftAuthenticationProvider();
    }

    @Bean
    UserDetailsRepository userDetailsRepository() {
        return new CraftUserDetailsRepositoryImpl();
    }

    @Bean
    LogoutSuccessHandler logoutSuccessHandler() {
        return new CraftLogoutHandler();
    }
}
