package craft.beer.management.service;

public interface CategoryService {
    enum CategoryCode {OK , CATEGORY_IN_USE}

    CategoryCode deleteCategory(Long catId);
}
