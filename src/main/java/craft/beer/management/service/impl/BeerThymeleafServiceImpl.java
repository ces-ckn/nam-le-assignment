package craft.beer.management.service.impl;

import com.google.common.collect.Lists;
import craft.beer.management.entity.data.Beer;
import craft.beer.management.entity.data.Category;
import craft.beer.management.repository.BeerRepository;
import craft.beer.management.repository.CategoryRepository;
import craft.beer.management.service.ThymeleafAbstractService;
import craft.beer.management.service.sdo.FragmentRequest;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

public class BeerThymeleafServiceImpl extends ThymeleafAbstractService {
    @Resource
    private BeerRepository beerRepository;

    @Resource
    private CategoryRepository categoryRepository;

    public BeerThymeleafServiceImpl() {
        controlBindingId = "cbeer_name";
    }

     @Override
    public void populateModelProperties(FragmentRequest request, Model model) {
        List<Beer> beers = Lists.newArrayList(beerRepository.findAll());
        Beer currentBeer = request.getCurrentId() == null ? beers.get(0) : getById(beers, request.getCurrentId());

        List<Category> categories = Lists.newArrayList(categoryRepository.findAll());

        model.addAttribute("beers", beers);
        model.addAttribute("id", currentBeer.getId());
        model.addAttribute("name", Objects.toString(currentBeer.getName(), ""));
        model.addAttribute("manufacture", currentBeer.getManufacture());
        model.addAttribute("country", Objects.toString(currentBeer.getCountry(), ""));
        model.addAttribute("price", Objects.toString(currentBeer.getPrice(), ""));
        model.addAttribute("description", currentBeer.getDescription());
        model.addAttribute("archive", currentBeer.getArchive());

        model.addAttribute("categories", categories);
        model.addAttribute("currentCategoryId", currentBeer.getCategoryId());
    }

    @Override
    protected void populateNewModelProperties(FragmentRequest request, Model model) {
        List<Category> categories = Lists.newArrayList(categoryRepository.findAll());
        Category currentCategory = request.getCurrentCategoryId() == null ? categories.get(0) : getById(categories, request.getCurrentCategoryId());
        model.addAttribute("categories", categories);
        model.addAttribute("currentCategoryId", currentCategory.getId());
    }
}
