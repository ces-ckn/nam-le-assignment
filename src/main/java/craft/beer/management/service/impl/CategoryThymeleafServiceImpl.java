package craft.beer.management.service.impl;

import com.google.common.collect.Lists;
import craft.beer.management.entity.data.Category;
import craft.beer.management.repository.CategoryRepository;
import craft.beer.management.service.ThymeleafAbstractService;
import craft.beer.management.service.sdo.FragmentRequest;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

public class CategoryThymeleafServiceImpl extends ThymeleafAbstractService {
    @Resource
    private CategoryRepository categoryRepository;

    public CategoryThymeleafServiceImpl() {
        controlBindingId = "cbeer_category";
    }

    @Override
    protected void populateModelProperties(FragmentRequest request, Model model) {
        List<Category> categories = Lists.newArrayList(categoryRepository.findAll());
        Category currentCategory = request.getCurrentId() == null ? categories.get(0) : getById(categories, request.getCurrentId());

        model.addAttribute("categories", categories);
        model.addAttribute("id", currentCategory.getId());
        model.addAttribute("name", Objects.toString(currentCategory.getName(), ""));
        model.addAttribute("description", currentCategory.getDescription());
        model.addAttribute("currentId", currentCategory.getId());
    }
}
