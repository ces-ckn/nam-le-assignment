package craft.beer.management.service.impl;

import com.google.common.collect.Lists;
import craft.beer.management.entity.api.ApiSerializationBeer;
import craft.beer.management.entity.data.Beer;
import craft.beer.management.entity.data.Category;
import craft.beer.management.entity.data.User;
import craft.beer.management.entity.data.UserBeer;
import craft.beer.management.repository.BeerRepository;
import craft.beer.management.repository.CategoryRepository;
import craft.beer.management.repository.UserBeerRepository;
import craft.beer.management.repository.UserRepository;
import craft.beer.management.service.BeerService;
import craft.beer.management.service.CraftAbstractService;
import org.apache.commons.lang3.BooleanUtils;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeerServiceImpl extends CraftAbstractService implements BeerService {
    private static final String BEER_DRANK_LIST = "drank";
    private static final String BEER_TRY_LIST = "shouldTry";

    @Resource
    private BeerRepository beerRepository;

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private UserBeerRepository userBeerRepository;

    @Resource
    private UserRepository userRepository;

    @Override
    public List<ApiSerializationBeer> getNoArchiveBeers() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<Beer> beers = Lists.newArrayList(beerRepository.findNoArchiveBeers());
        return create(beers, indexCategoryNameById());
    }

    @Override
    public UserBeer userDrinkOneBeer(Long userId, Long beerId) {
        UserBeer userBeer = new UserBeer();
        userBeer.setUserId(userId);
        userBeer.setBeerId(beerId);
        return userBeerRepository.save(userBeer);
    }

    @Override
    public Map<String, List<ApiSerializationBeer>> getCustomerBeers(Long userId) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<Beer> allBeers = Lists.newArrayList(beerRepository.findAll());
        User user = userRepository.findOne(userId);

        return generateBeerMap(allBeers, user.getUserBeers(), indexCategoryNameById());
    };

    private List<ApiSerializationBeer> create(List<Beer> beers, Map<Long, String> categoriesById) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        // Transform a list of Beer to ApiSerializationBeer
        List<ApiSerializationBeer> beerList = new ArrayList<>();
        for (Beer beer : beers) {
            beerList.add(ApiSerializationBeer.from(beer, categoriesById.get(beer.getCategoryId())));
        }
        return beerList;
    }

    // Create Map<Id, categoryName>
    private Map<Long, String> indexCategoryNameById() {
        List<Category> categories = Lists.newArrayList(categoryRepository.findAll());
        Map<Long, String> categoryNamesById =  new HashMap<>();

        for (Category category : categories) {
            categoryNamesById.put(category.getId(), category.getName());
        }

        return categoryNamesById;
    }

    private Map<String, List<ApiSerializationBeer>> generateBeerMap(List<Beer> allBeers, List<UserBeer> drankUserBeers, Map<Long, String> categoriesNameById) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Map<String, List<ApiSerializationBeer>> result = new HashMap<>();
        List<ApiSerializationBeer> drankApiBeers = new ArrayList<>();
        List<ApiSerializationBeer> noneDrankApiBeers = new ArrayList<>();
        Map<Long, UserBeer> userBeersById = indexById(drankUserBeers);

        // Separate two category beers: one which has been drunk and not drunk
        // Convert Beer entity to ApiBeer entity
        for (Beer beer : allBeers) {
            if (userBeersById.containsKey(beer.getId())) {
                drankApiBeers.add(ApiSerializationBeer.from(beer, categoriesNameById.get(beer.getCategoryId())));
            } else if (BooleanUtils.isNotTrue(beer.getArchive())) {
                // only put beer that is not archived
                noneDrankApiBeers.add(ApiSerializationBeer.from(beer, categoriesNameById.get(beer.getCategoryId())));
            }
        }

        // Put two categories in map
        result.put(BEER_DRANK_LIST, drankApiBeers);
        result.put(BEER_TRY_LIST, noneDrankApiBeers);

        return result;
    }
}
