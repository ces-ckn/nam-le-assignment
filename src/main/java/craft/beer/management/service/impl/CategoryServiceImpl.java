package craft.beer.management.service.impl;

import craft.beer.management.repository.BeerRepository;
import craft.beer.management.repository.CategoryRepository;
import craft.beer.management.service.CategoryService;

import javax.annotation.Resource;

public class CategoryServiceImpl implements CategoryService {
    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private BeerRepository beerRepository;

    /**
     * Delete a category when it doesn't contain any beers. If not return an error code
     * @param catId
     * @return
     */
    @Override
    public CategoryCode deleteCategory(Long catId) {
        CategoryCode returnCode = CategoryCode.OK;

        int numberBeerInUser = beerRepository.countBeerInCategory(catId);
        if (numberBeerInUser == 0) {
            categoryRepository.delete(catId);
        } else {
            returnCode = CategoryCode.CATEGORY_IN_USE;
        }

        return returnCode;
    }
}
