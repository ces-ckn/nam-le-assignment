package craft.beer.management.service.impl;

import com.google.common.collect.Lists;
import craft.beer.management.entity.data.Role;
import craft.beer.management.entity.data.User;
import craft.beer.management.entity.data.UserRole;
import craft.beer.management.repository.RoleRepository;
import craft.beer.management.repository.UserRepository;
import craft.beer.management.repository.UserRoleRepository;
import craft.beer.management.service.ThymeleafAbstractService;
import craft.beer.management.service.sdo.FragmentRequest;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserThymeleafServiceImpl extends ThymeleafAbstractService {
    @Resource
    private UserRepository userRepository;

    @Resource
    private UserRoleRepository userRoleRepository;

    @Resource
    private RoleRepository roleRepository;

    public UserThymeleafServiceImpl() {
        controlBindingId = "cbeer_user";
    }

    @Override
    protected void populateModelProperties(FragmentRequest request, Model model) {
        List<User> users = Lists.newArrayList(userRepository.findAll());
        User currentUser = request.getCurrentId() == null ? users.get(0) : getById(users, request.getCurrentId());
        List<Role> roles = Lists.newArrayList(roleRepository.findAll());

        model.addAttribute("users", users);
        model.addAttribute("id", currentUser.getId());
        model.addAttribute("password", currentUser.getPassword());
        model.addAttribute("firstName", Objects.toString(currentUser.getFirstName(), " "));
        model.addAttribute("lastName", Objects.toString(currentUser.getLastName(), " "));
        model.addAttribute("email", Objects.toString(currentUser.getEmail(), " "));
        model.addAttribute("currentId", currentUser.getId());
        model.addAttribute("username", currentUser.getUsername());

        model.addAttribute("roles", roles);
        model.addAttribute("currentRoleIds", getRoleIdFromUserRoles(currentUser.getUserRoles()));
    }

    @Override
    protected void populateNewModelProperties(FragmentRequest request, Model model) {
        List<Role> roles = Lists.newArrayList(roleRepository.findAll());
        model.addAttribute("roles", roles);
        model.addAttribute("currentRoleIds", new ArrayList<UserRole>());
    }

    // Convert a list of entities to a map of ID, entities
    protected List<Long> getRoleIdFromUserRoles(List<UserRole> entities) {
        List<Long> rolesId = new ArrayList<>();

        for (UserRole entity : entities) {
            rolesId.add(entity.getRoleId());
        }

        return rolesId;
    }
}
