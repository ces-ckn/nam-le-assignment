package craft.beer.management.service;

import craft.beer.management.entity.CraftEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CraftAbstractService {
    // Convert a list of entities to a map of ID, entities
    protected <T extends CraftEntity> Map<Long, T> indexById(List<T> entities) {
        Map<Long, T> entityById = new HashMap<>();

        for (T entity : entities) {
            entityById.put(entity.getId(), entity);
        }

        return entityById;
    }
}
