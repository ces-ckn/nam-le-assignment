package craft.beer.management.service;

import craft.beer.management.entity.CraftEntity;
import craft.beer.management.service.sdo.FragmentRequest;
import org.springframework.ui.Model;

import java.util.List;

public abstract class ThymeleafAbstractService {
    private static String ACTION_NEW = "new";
    private static String ACTION_EDIT = "edit";
    private static String MODE_LOAD = "load";

    protected String controlBindingId = "control-id";

    public void populateFragmentModel(FragmentRequest request, Model model) {
        String mode = ACTION_NEW.equalsIgnoreCase(request.getAction()) || ACTION_EDIT.equalsIgnoreCase(request.getAction())
                ? ACTION_EDIT : MODE_LOAD;
        model.addAttribute("mode", mode); // Mode 'edit' component on ui can be editable
        model.addAttribute("cancel", true);

        if (!ACTION_NEW.equalsIgnoreCase(request.getAction())) {
            populateModelProperties(request, model);

            if (ACTION_EDIT.equalsIgnoreCase(request.getAction())) {
                model.addAttribute("forControlId", getControlBindingId() + "_" + ACTION_NEW);
            } else {
                model.addAttribute("forControlId", getControlBindingId());
                model.addAttribute("disabled", true);
                model.addAttribute("cancel", false);
            }
        } else {
            model.addAttribute("forControlId", getControlBindingId() + "_" + ACTION_NEW);
            populateNewModelProperties(request, model);
        }
    }

    protected abstract void populateModelProperties(FragmentRequest request, Model model);

    protected void populateNewModelProperties(FragmentRequest request, Model model) {
    }

    protected String getControlBindingId() {
        return controlBindingId;
    }

    // Get one entity in a list by id
    public <T extends CraftEntity> T getById(List<T> entities, Long id) {
        for (T entity : entities) {
            if (id.longValue() == entity.getId()) {
                return entity;
            }
        }
        return null;
    }
}
