package craft.beer.management.service;

import craft.beer.management.entity.api.ApiSerializationBeer;
import craft.beer.management.entity.data.UserBeer;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

public interface BeerService {
    List<ApiSerializationBeer> getNoArchiveBeers() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException;

    UserBeer userDrinkOneBeer(Long userId, Long beerId);

    Map<String, List<ApiSerializationBeer>> getCustomerBeers(Long userId) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException;
}
