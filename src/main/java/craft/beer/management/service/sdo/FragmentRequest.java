package craft.beer.management.service.sdo;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class FragmentRequest {
    String action;
    Long currentId;
    Long currentCategoryId;
}
