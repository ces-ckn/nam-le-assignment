package craft.beer.management.controller;

import com.google.common.collect.Lists;
import craft.beer.management.entity.api.ApiSerializationBeer;
import craft.beer.management.entity.data.UserBeer;
import craft.beer.management.entity.data.UserRole;
import craft.beer.management.repository.UserRoleRepository;
import craft.beer.management.service.BeerService;
import craft.beer.management.service.CategoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import static craft.beer.management.service.CategoryService.CategoryCode.CATEGORY_IN_USE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class ApiController {
    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    BeerService beerService;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    CategoryService categoryService;

    /**
     * Obtain a list of beers that are still selling in the bar
     *
     * @return
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    @RequestMapping(value = "/api/anyone/beers", method = GET)
    public ResponseEntity getBeerList() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<ApiSerializationBeer> beers = beerService.getNoArchiveBeers();
        return new ResponseEntity<>(beers, HttpStatus.OK);
    }


    /**
     * Customer api to add beer that he has drank or tried
     * <p/>
     * (Must be POST only but adding GET for testing with out authentication)
     *
     * @return
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    @RequestMapping(value = "/api/customers/{userId}/drink/{beerId}", method = {GET, POST})
    public ResponseEntity customerDrinkBeer(@PathVariable("userId") String userId,
                                            @PathVariable("beerId") String beerId
    ) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        UserBeer userBeer = beerService.userDrinkOneBeer(parseId(userId), parseId(beerId));
        return new ResponseEntity<>(userBeer, HttpStatus.CREATED);
    }

    /**
     * Get list beers from customer. Will obtain a map of two lists:
     * drank: contains beer customer has drank no matter it is archived or not
     * shouldTry: contains beer user hasn't been drank but still selling (not archive)
     *
     * @param userId
     * @return
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    @RequestMapping(value = "/api/customers/{userId}/beers", method = GET)
    public ResponseEntity getCustomerBeers(@PathVariable("userId") String userId)
        throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {

        Map<String, List<ApiSerializationBeer>> beers = beerService.getCustomerBeers(parseId(userId));
        return new ResponseEntity<>(beers, HttpStatus.OK);
    }

    /**
     * Override creating a list of user roles
     * @param roleAuthorities
     * @return
     */
    @RequestMapping(value = "/api/userRoles", method = RequestMethod.POST)
    public ResponseEntity<List<UserRole>> saveUserRoles(@RequestBody List<UserRole> roleAuthorities) {
        List<UserRole> currentUserRoles = userRoleRepository.findRolesByUserId(roleAuthorities.get(0).getUserId());
        userRoleRepository.delete(currentUserRoles);
        List<UserRole> savedUserRoles = Lists.newArrayList(userRoleRepository.save(roleAuthorities));

        return new ResponseEntity<>(savedUserRoles, HttpStatus.CREATED);
    }

    /**
     * Delete a category safely only when no beers use this category
     * @param catId
     * @return
     */
    @RequestMapping(value = "/api/categories/safe/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<List<UserRole>> deleteCategory(@PathVariable("id") String catId) {
        HttpStatus status = HttpStatus.NO_CONTENT;
        if (CATEGORY_IN_USE.equals(categoryService.deleteCategory(parseId(catId)))) {
            status = HttpStatus.FAILED_DEPENDENCY;
        }

        return new ResponseEntity<>(status);
    }

    private Long parseId(String id) {
        return StringUtils.isBlank(id) ? null : Long.parseLong(id);
    }
}
