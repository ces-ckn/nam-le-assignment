package craft.beer.management.controller;

import craft.beer.management.security.user.impl.CraftUserDetailsImpl;
import craft.beer.management.security.user.impl.CraftUserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static craft.beer.management.security.role.CraftRoles.ROLE_ADMIN;

@Controller
public class LoginController {
    @Autowired
    ApplicationContext applicationContext;

    @RequestMapping(value = {"/*", "/"})
    public String getPage(Model model, HttpServletRequest request) {
        CraftUserDetailsImpl userDetails = CraftUserDetailsServiceImpl.getUserDetails();

        if (userDetails != null) {
            model.addAttribute("hasAuthenticated", true);
            model.addAttribute("role", userDetails.getAuthorities().get(0).getAuthority());

            String defaultPage = request.isUserInRole(ROLE_ADMIN.name()) ? "user" : "customer";
            model.addAttribute("defaultPage", defaultPage);
        }

        return userDetails != null ? "landing-page" : "login";
    }
}
