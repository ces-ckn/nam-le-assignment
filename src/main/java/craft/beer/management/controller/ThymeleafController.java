package craft.beer.management.controller;

import craft.beer.management.security.role.CraftRoles;
import craft.beer.management.security.user.impl.CraftUserDetailsImpl;
import craft.beer.management.service.ThymeleafAbstractService;
import craft.beer.management.security.user.impl.CraftUserDetailsServiceImpl;
import craft.beer.management.service.sdo.FragmentRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

import static craft.beer.management.security.role.CraftRoles.ROLE_ADMIN;
import static org.springframework.http.HttpStatus.FAILED_DEPENDENCY;

@Controller
public class ThymeleafController {
    @Autowired
    ApplicationContext appContext;

    @Autowired
    UserDetailsService craftUserDetailsService;

    @Autowired
    ThymeleafAbstractService beerThymeleafService;

    @Autowired
    ThymeleafAbstractService userThymeleafService;

    @Autowired
    ThymeleafAbstractService categoryThymeleafService;

    /**
     * Error page
     * @param model
     * @param code
     * @return
     */
    @RequestMapping(value = "fragment/error")
    public String getErrorPage(Model model, @RequestParam(value = "code", defaultValue = "") String code) {
        CraftUserDetailsImpl userDetails = CraftUserDetailsServiceImpl.getUserDetails();

        if (userDetails != null && userDetails.getAuthorities().contains(CraftRoles.ROLE_ADMIN)
            && FAILED_DEPENDENCY.getReasonPhrase().equals(code)) {
            model.addAttribute("errorMessage", appContext.getMessage("error.messages.category.inuse", null, Locale.getDefault()));
        } else {
            model.addAttribute("errorMessage", appContext.getMessage("error.messages.general", null, Locale.getDefault()));
        }

        return userDetails != null ? "error :: content" : "login";
    }

    /**
     * Empty page for customer
     * @param model
     * @return
     */
    @RequestMapping(value = "fragment/customer")
    public String getPage(Model model) {
        CraftUserDetailsImpl userDetails = CraftUserDetailsServiceImpl.getUserDetails();

        if (userDetails != null) {
            model.addAttribute("hasAuthenticated", true);
        }

        return userDetails != null ? "customer :: content" : "login";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "fragment/user")
    public String getUserPage(Model model,
                              @RequestParam(value = "action", defaultValue = "") String action,
                              @RequestParam(value = "currentId", defaultValue = "") String currentId,
                              HttpServletRequest request) {
        if (!request.isUserInRole(ROLE_ADMIN.name())) {
            return "";
        }
        setPrivateSectionModel(model, "user");
        FragmentRequest fragmentRequest = new FragmentRequest(action, parseId(currentId), null);
        userThymeleafService.populateFragmentModel(fragmentRequest, model);
        return "user :: content";
    }

    @RequestMapping(value = "fragment/beer")
    public String getBeerPage(Model model,
                              @RequestParam(value = "action", defaultValue = "") String action,
                              @RequestParam(value = "currentId", defaultValue = "") String currentId,
                              @RequestParam(value = "categoryId", defaultValue = "") String currentCategoryId,
                              HttpServletRequest request) {
        if (!request.isUserInRole(ROLE_ADMIN.name())) {
            return "";
        }
        setPrivateSectionModel(model, "beer");
        FragmentRequest fragmentRequest = new FragmentRequest(action, parseId(currentId), parseId(currentCategoryId));
        beerThymeleafService.populateFragmentModel(fragmentRequest, model);
        return "beer :: content";
    }

    @RequestMapping(value = "fragment/category")
    public String getCategoryPage(Model model,
                                  @RequestParam(value = "action", defaultValue = "") String action,
                                  @RequestParam(value = "currentId", defaultValue = "") String currentId,
                                  HttpServletRequest request) {
        if (!request.isUserInRole(ROLE_ADMIN.name())) {
            return "";
        }
        setPrivateSectionModel(model, "category");
        FragmentRequest fragmentRequest = new FragmentRequest(action, parseId(currentId), null);
        categoryThymeleafService.populateFragmentModel(fragmentRequest, model);
        return "category :: content";
    }

    private void setPrivateSectionModel(Model model, String view) {
        CraftUserDetailsImpl userDetails = CraftUserDetailsServiceImpl.getUserDetails();

        if (userDetails != null) {
            model.addAttribute("role", userDetails.getAuthorities().get(0).getAuthority());
            model.addAttribute("sectionTitle", getTitle(view));
            model.addAttribute("hasAuthenticated", true);
        }
    }

    private String getTitle(String page) {
        return StringUtils.isNotEmpty(page) ? appContext.getMessage("section.title." + page, null, Locale.getDefault()) : "";
    }

    private Long parseId(String id) {
        return StringUtils.isBlank(id) ? null : Long.parseLong(id);
    }
}
