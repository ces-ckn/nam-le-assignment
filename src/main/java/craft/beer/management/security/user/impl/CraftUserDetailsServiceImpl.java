package craft.beer.management.security.user.impl;

import craft.beer.management.security.user.UserDetailsRepository;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CraftUserDetailsServiceImpl implements UserDetailsService {
    @Resource
    private UserDetailsRepository userDetailsRepository;

    @Setter
    Map<Integer, ? extends GrantedAuthority> rolesById;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Map<String, Object> userData = userDetailsRepository.findUserDataByUsername(username);
        CraftUserDetailsImpl userDetails;

        // Create UserDetails from database Map properties
        if (userData != null) {
            userDetails = CraftUserDetailsImpl.from(userData);
        } else {
            throw new UsernameNotFoundException("[" + username + "] not found");
        }

        // Convert a list of role ids to a list of GrantedAuthority
        @SuppressWarnings("unchecked") Set<Number> rolesIds;
        try {
            rolesIds = asNumberSet((Array) userData.get("authorities"));
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }

        // Set user authorities
        userDetails.setAuthorities(mapAuthorities(rolesIds));
        return userDetails;
    }

    public static CraftUserDetailsImpl getUserDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated() && authentication.getPrincipal() instanceof UserDetails) {
            return (CraftUserDetailsImpl) authentication.getPrincipal();
        } else {
            return null;
        }
    }

    private List<? extends GrantedAuthority> mapAuthorities(Set<Number> rolesId) {
        List<GrantedAuthority> roles = new ArrayList<>();
        // Convert from role ids to granted authorities
        for (Number id : rolesId) {
            if (rolesById.containsKey(id.intValue())) {
                roles.add(rolesById.get(id.intValue()));
            }
        }
        return roles;
    }

    private HashSet<Number> asNumberSet(Array sqlArray) throws SQLException {
        return new HashSet<>(Arrays.asList((Number[]) sqlArray.getArray()));
    }
}
