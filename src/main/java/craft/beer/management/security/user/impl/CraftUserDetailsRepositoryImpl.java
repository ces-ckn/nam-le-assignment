package craft.beer.management.security.user.impl;

import craft.beer.management.security.user.UserDetailsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CraftUserDetailsRepositoryImpl implements UserDetailsRepository {

    private static Logger log = LoggerFactory.getLogger(CraftUserDetailsRepositoryImpl.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Map<String, Object> findUserDataByUsername(String username) {
        Map<String, Object> result = null;
        // Query for user data information including a list of role ids
        String sql = "SELECT usr_id as userId, " +
                "            usr_account as username, " +
                "            usr_password as password, " +
                "            usr_first_name as firstName, " +
                "            usr_last_name as lastName, " +
                "            usr_email as email, " +
                "            array_agg(rol_id) as authorities, " +
                "            usr_valid_to IS NULL OR usr_valid_to >= CURRENT_DATE as accountNoneExpired " +
                "     FROM craft_beer.user usr " +
                "            LEFT JOIN  craft_beer.user_role urol ON usr_id = urol_usr_id " +
                "            LEFT JOIN  craft_beer.role role ON urol_rol_id = rol_id " +
                "     WHERE usr_account = :username " +
                "            AND (rol_valid_to IS NULL OR rol_valid_to >= CURRENT_DATE) " +
                "     GROUP BY usr_id, usr_account, usr_password, usr_first_name, usr_last_name, usr_email, accountNoneExpired ";

        Map<String, Object> params = new HashMap<>();
        params.put("username", username);
        List<Map<String, Object>> userData = jdbcTemplate.queryForList(sql, params);

        // If no user data or more than 1 user has been found, return null
        if (userData.isEmpty()) {
            log.error("No corresponding user found for username '{}'", username);
        } else if (userData.size() > 1) {
            log.error("Too many matches found for username '{}'", username);
        } else {
            result = userData.iterator().next();
        }

        return result;
    }
}
