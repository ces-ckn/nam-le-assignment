package craft.beer.management.security.user;

import java.util.Map;

public interface UserDetailsRepository {
    Map<String, Object> findUserDataByUsername(String username);
}
