package craft.beer.management.security.user.impl;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Data
public class CraftUserDetailsImpl implements UserDetails {
    private String username;
    private Integer userId;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private boolean isAccountNonExpired = true;
    private boolean isAccountNonLocked = true;
    private boolean isCredentialsNonExpired = true;
    private boolean isEnabled = true;
    private List<? extends GrantedAuthority> authorities;

    public static CraftUserDetailsImpl of(UserDetails userDetails) {
        if (userDetails instanceof CraftUserDetailsImpl) {
            return (CraftUserDetailsImpl) userDetails;
        } else {
            throw new IllegalArgumentException("This is not a Craft Beer User Details implementation");
        }
    }

    /**
     * Create a CraftUserDetailsImpl object from a Map of properties. Mostly this Map is returned from a database query.
     * @param data
     * @return
     */
    public static CraftUserDetailsImpl from(Map<String, Object> data) {
        CraftUserDetailsImpl userDetails = new CraftUserDetailsImpl();
        userDetails.setUserId(Integer.valueOf(data.get("userId").toString()));
        userDetails.setUsername(data.get("username").toString());
        userDetails.setFirstName(Objects.toString(data.get("firstName"), ""));
        userDetails.setLastName(Objects.toString(data.get("lastName"), ""));
        userDetails.setEmail(Objects.toString(data.get("email"), ""));
        userDetails.setAccountNonExpired(Boolean.TRUE.equals(data.get("accountNoneExpired")));
        // todo: encode and decode password but don't have enough time now
        userDetails.setPassword((data.get("password")).toString());

        return userDetails;
    }
}
