package craft.beer.management.security.role;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum CraftRoles implements GrantedAuthority {
    ROLE_ADMIN                                   (1),
    ROLE_CUSTOMER                                (2);

    @Getter
    private Integer id;

    @Override
    public String getAuthority() {
        return name();
    }

    public static Map<Integer, CraftRoles> byId() {
        Map<Integer, CraftRoles> rolesById = new HashMap<>();
        // Create a map with key is role id, value is type of GrantedAuthority
        for (CraftRoles role : values()) {
            rolesById.put(role.getId(), role);
        }
        return rolesById;
    }
}
