package craft.beer.management.security.authentication;

import craft.beer.management.security.user.impl.CraftUserDetailsImpl;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;

public class CraftAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Resource
    private UserDetailsService userDetailsService;

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        try {
            return userDetailsService.loadUserByUsername(username);
        } catch (UsernameNotFoundException cause) {
            throw cause;
        }
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        CraftUserDetailsImpl user = CraftUserDetailsImpl.of(userDetails);
        if (!authentication.getCredentials().equals(user.getPassword())) {
            throw new BadCredentialsException("badCredentials");
        }
    }

    @Override
    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails userDetails) {
        return super.createSuccessAuthentication(principal, authentication, userDetails);
    }
}
