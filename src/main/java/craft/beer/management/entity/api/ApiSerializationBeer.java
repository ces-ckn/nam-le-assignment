package craft.beer.management.entity.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import craft.beer.management.entity.data.Beer;
import craft.beer.management.entity.data.UserBeer;
import lombok.Data;
import org.apache.commons.lang3.BooleanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static org.apache.commons.beanutils.PropertyUtils.*;

/**
 * Beer entity contain useful information
 */
@Data
public class ApiSerializationBeer extends Beer {
    private static final String BEER_SELLING = "SELLING";
    private static final String BEER_ARCHIVE = "STOPPED SELLING";

    @JsonIgnore
    private long id;

    @JsonIgnore
    private Boolean archive;

    @JsonProperty("status")
    private String getStatus() {
        return (BooleanUtils.isTrue(archive) ? BEER_ARCHIVE : BEER_SELLING);
    }

    @JsonIgnore
    private Long categoryId;

    @JsonIgnore
    private String categoryName;

    @JsonProperty("category")
    public String getCategoryName() {
        return categoryName;
    }

    @JsonIgnore
    private List<UserBeer> userBeers;

    public static ApiSerializationBeer from(Beer beer, String categoryName) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        ApiSerializationBeer apiBeer = new ApiSerializationBeer();
        copyProperties(apiBeer, beer);
        apiBeer.setCategoryName(categoryName);
        return apiBeer;
    }
}
