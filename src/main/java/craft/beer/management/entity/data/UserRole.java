package craft.beer.management.entity.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import craft.beer.management.entity.CraftEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@Entity
@Table(schema = "craft_beer", name = "user_role")
public class UserRole implements CraftEntity {
    public static final String DELETE_ROLES_BY_USER = "SELECT * FROM craft_beer.user_role WHERE urol_usr_id = :userId";

    @Id
    @Column(name = "urol_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "uroIdSequence")
    @SequenceGenerator(name = "uroIdSequence", sequenceName = "craft_beer.seq_user_role_id", allocationSize = 1)
    private long id;

    @Column(name = "urol_usr_id")
    private long userId;

    @Column(name = "urol_rol_id")
    private long roleId;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "urol_usr_id", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "urol_rol_id", insertable = false, updatable = false)
    private Role role;
}
