package craft.beer.management.entity.data;

import craft.beer.management.entity.CraftEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(schema = "craft_beer", name = "role")
public class Role implements CraftEntity {
    @Id
    @Column(name = "rol_id")
    private long id;

    @Column(name = "rol_name")
    private String name;

    @Column(name = "rol_valid_from")
    private Date validFrom = new Date();

    @Column(name = "rol_valid_to")
    private Date validTo;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserRole> userRoles;
}
