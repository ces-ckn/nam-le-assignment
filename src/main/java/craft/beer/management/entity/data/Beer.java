package craft.beer.management.entity.data;

import craft.beer.management.entity.CraftEntity;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(schema = "craft_beer", name = "beer")
public class Beer implements CraftEntity {
    public static final String ANYONE_QUERY = "SELECT * FROM craft_beer.beer WHERE beer_archive is not true";

    @Id
    @Column(name = "beer_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "beerIdSequence")
    @SequenceGenerator(name = "beerIdSequence", sequenceName = "craft_beer.seq_beer_id", allocationSize = 1)
    protected long id;

    @Column(name = "beer_name")
    protected String name;

    @Column(name = "beer_manufacture")
    protected String manufacture;

    @Column(name = "beer_country_code")
    protected String country;

    @Column(name = "beer_price")
    protected Double price;

    @Column(name = "beer_description")
    protected String description = "";

    @Column(name = "beer_archive")
    protected Boolean archive = false;

    @Column(name = "beer_cat_id")
    protected Long categoryId;

    @ManyToOne
    @JoinColumn(name = "beer_cat_id", insertable = false, updatable = false)
    private Category category;

    @OneToMany(mappedBy = "beer", cascade = CascadeType.ALL, orphanRemoval = false)
    private List<UserBeer> userBeers;
}
