package craft.beer.management.entity.data;

import craft.beer.management.entity.CraftEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "craft_beer", name = "user_beer")
public class UserBeer implements CraftEntity {
    @Id
    @Column(name = "ubeer_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "uBeerIdSequence")
    @SequenceGenerator(name = "uBeerIdSequence", sequenceName = "craft_beer.seq_user_beer_id", allocationSize = 1)
    private long id;

    @Column(name = "ubeer_usr_id")
    private long userId;

    @Column(name = "ubeer_beer_id")
    private long beerId;

    @ManyToOne
    @JoinColumn(name = "ubeer_usr_id", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "ubeer_beer_id", insertable = false, updatable = false)
    private Beer beer;
}
