package craft.beer.management.entity.data;

import craft.beer.management.entity.CraftEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(schema = "craft_beer", name = "category")
public class Category implements CraftEntity {
    public static final String CATEGORY_IN_USE_QUERY = "SELECT count(*) FROM craft_beer.beer WHERE beer_cat_id = :catId and beer_archive is not true";

    @Id
    @Column(name = "cat_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "catIdSequence")
    @SequenceGenerator(name = "catIdSequence", sequenceName = "craft_beer.seq_category_id", allocationSize = 1)
    private long id;

    @Column(name = "cat_name")
    private String name;

    @Column(name = " cat_description")
    private String description;

    @OneToMany(mappedBy = "category")
    private List<Beer> beers;
}
