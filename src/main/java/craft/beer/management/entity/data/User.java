package craft.beer.management.entity.data;

import craft.beer.management.entity.CraftEntity;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(schema = "craft_beer", name = "user")
public class User implements CraftEntity {
    @Id
    @Column(name = "usr_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userIdSequence")
    @SequenceGenerator(name = "userIdSequence", sequenceName = "craft_beer.seq_user_id", allocationSize = 1)
    private long id;

    @Column(name = "usr_account")
    private String username;

    @Column(name = "usr_password")
    private String password;

    @Column(name = "usr_first_name")
    private String firstName;

    @Column(name = "usr_last_name")
    private String lastName;

    @Column(name = "usr_valid_from")
    private Date validFrom = new Date();

    @Column(name = "usr_valid_to")
    private Date validTo;

    @Column(name = "usr_email")
    private String email;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserRole> userRoles;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserBeer> userBeers;
}
